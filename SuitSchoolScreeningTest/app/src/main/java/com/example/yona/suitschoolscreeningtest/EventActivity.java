package com.example.yona.suitschoolscreeningtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class EventActivity extends AppCompatActivity {
    ArrayList<Event> events;
    EventAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        events = new ArrayList<>();
        ListView listView = (ListView) findViewById(R.id.listEvent);
        tambahProduk();
        adapter = new EventAdapter(getApplicationContext(), events);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(events.get(position).getNama() == "Andi")
                {
                    Intent intent = new Intent(EventActivity.this, NextActivity.class);
                    intent.putExtra("nama", "Andi");
                    startActivity(intent);
                }
                else if(events.get(position).getNama() == "Budi")
                {
                    Intent intent = new Intent(EventActivity.this, NextActivity.class);
                    intent.putExtra("nama", "Budi");
                    startActivity(intent);
                }
                else if(events.get(position).getNama() == "Charlie")
                {
                    Intent intent = new Intent(EventActivity.this, NextActivity.class);
                    intent.putExtra("nama", "Charlie");
                    startActivity(intent);
                }
                else if(events.get(position).getNama() == "Dede")
                {
                    Intent intent = new Intent(EventActivity.this, NextActivity.class);
                    intent.putExtra("nama", "Dede");
                    startActivity(intent);
                }
                else if(events.get(position).getNama() == "Joko")
                {
                    Intent intent = new Intent(EventActivity.this, NextActivity.class);
                    intent.putExtra("nama", "Joko");
                    startActivity(intent);
                }
            }
        });
    }

    void tambahProduk() {
        Event p1 = new Event("Andi", "2014-01-01");
        events.add(p1);

        Event p2 = new Event("Budi", "2014-02-02");
        events.add(p2);

        Event p3 = new Event("Charlie", "2014-03-03");
        events.add(p3);

        Event p4 = new Event("Dede", "2014-06-06");
        events.add(p4);

        Event p5 = new Event("Joko", "2014-02-12");
        events.add(p5);
    }
}