package com.example.yona.suitschoolscreeningtest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private EditText username = null;
    private Button btnNext;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private final String KEY_NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        username = (EditText) findViewById(R.id.username);
        btnNext = (Button) findViewById(R.id.buttonNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = username.getText().toString();
                    editor = pref.edit();
                    editor.putString("name",name);
                    editor.commit();
                    Intent nextScreen = new Intent(getApplicationContext(), NextActivity.class);
                    startActivity(nextScreen);
            }

        });
    }
}
