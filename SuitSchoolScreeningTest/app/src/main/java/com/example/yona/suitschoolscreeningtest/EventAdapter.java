package com.example.yona.suitschoolscreeningtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Yona on 2/22/2017.
 */
public class EventAdapter extends ArrayAdapter<Event> {
    Context context;
    ArrayList<Event> events;

    static class ViewHolder {
        public TextView nama;
        public TextView tanggal;
        public ImageView gambar;
    }

    public EventAdapter(Context context, ArrayList<Event> produks) {
        super(context, R.layout.event_item, produks);
        this.context = context;
        this.events = events;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Event event = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.event_item, parent, false);
            viewHolder.nama = (TextView) convertView.findViewById(R.id.nama);
            viewHolder.tanggal = (TextView) convertView.findViewById(R.id.tanggal);
            viewHolder.gambar = (ImageView) convertView.findViewById(R.id.gambar);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nama.setText(event.getNama());
        viewHolder.tanggal.setText(event.getTanggal());
        viewHolder.gambar.setImageResource(R.drawable.gambar);
        return convertView;
    }
}
