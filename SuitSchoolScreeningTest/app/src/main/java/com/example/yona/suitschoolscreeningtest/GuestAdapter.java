package com.example.yona.suitschoolscreeningtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Yona on 2/22/2017.
 */
public class GuestAdapter extends BaseAdapter {
    private Context context;
    private final String[] events;

    public GuestAdapter(Context context, String[] events) {
        this.context = context;
        this.events = events;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.guest_item, null);

            // set value into textview
            TextView nama = (TextView) gridView
                    .findViewById(R.id.nama);
            nama.setText(events[position]);

            TextView tanggal = (TextView) gridView
                    .findViewById(R.id.tanggal);
            tanggal.setText(events[position]);

            // set image based on selected text
            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.imageview);

            String event = events[position];

            if (event.equals("Andi")) {
                imageView.setImageResource(R.drawable.gambar);
                tanggal.setText("2014-01-01");
            } else if (event.equals("Budi")) {
                imageView.setImageResource(R.drawable.gambar);
                tanggal.setText("2014-02-02");
            } else if (event.equals("Charlie")) {
                imageView.setImageResource(R.drawable.gambar);
                tanggal.setText("2014-03-03");
            } else if (event.equals("Dede")) {
                imageView.setImageResource(R.drawable.gambar);
                tanggal.setText("2014-06-06");
            } else if (event.equals("Joko")) {
                imageView.setImageResource(R.drawable.gambar);
                tanggal.setText("2014-02-12");
            }

        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

    @Override
    public int getCount() {
        return events.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}