package com.example.yona.suitschoolscreeningtest;

/**
 * Created by Yona on 2/22/2017.
 */
public class Event {
    private String nama;
    private String tanggal;

    public Event(String nama, String tanggal) {
        this.nama = nama;
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
