package com.example.yona.suitschoolscreeningtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class GuestActivity extends AppCompatActivity {
    GridView gridView;

    static final String[] nama = new String[] {
            "Andi", "Budi","Charlie", "Dede", "Joko" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        gridView = (GridView) findViewById(R.id.gridview);

        gridView.setAdapter(new GuestAdapter(this, nama));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                if(position == 0)
                {
                    Intent intent = new Intent(GuestActivity.this, NextActivity.class);
                    intent.putExtra("namaGuest", "Andi");
                    startActivity(intent);
                    Toast.makeText(GuestActivity.this, "feature phone", Toast.LENGTH_SHORT).show();
                } else if(position == 1) {
                    Intent intent = new Intent(GuestActivity.this, NextActivity.class);
                    intent.putExtra("namaGuest", "Budi");
                    startActivity(intent);
                    Toast.makeText(GuestActivity.this, "blackberry", Toast.LENGTH_SHORT).show();
                } else if(position == 2) {
                    Intent intent = new Intent(GuestActivity.this, NextActivity.class);
                    intent.putExtra("namaGuest", "Charlie");
                    startActivity(intent);
                    Toast.makeText(GuestActivity.this, "android", Toast.LENGTH_SHORT).show();
                } else if(position == 3) {
                    Intent intent = new Intent(GuestActivity.this, NextActivity.class);
                    intent.putExtra("namaGuest", "Dede");
                    startActivity(intent);
                    Toast.makeText(GuestActivity.this, "iOS", Toast.LENGTH_SHORT).show();
                } else if(position == 4) {
                    Intent intent = new Intent(GuestActivity.this, NextActivity.class);
                    intent.putExtra("namaGuest", "Joko");
                    startActivity(intent);
                    Toast.makeText(GuestActivity.this, "iOS", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

}