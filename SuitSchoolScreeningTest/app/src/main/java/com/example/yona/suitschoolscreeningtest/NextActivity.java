package com.example.yona.suitschoolscreeningtest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class NextActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private  SharedPreferences.Editor editor;
    private final String KEY_NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = pref.getString("name", "username");
        TextView tvUsername = (TextView) findViewById(R.id.txtUsername);
        tvUsername.setText(username);
    }

    public void Event(View view){
        Button btnUsername = (Button) findViewById(R.id.buttonEvent);
        String type = getIntent().getStringExtra("nama");
        btnUsername.setText(type);
        Intent intent = new Intent(this, EventActivity.class);
        startActivity(intent);
    }

    public void Guest(View view){
        Button btnUsername = (Button) findViewById(R.id.buttonGuest);
        String type = getIntent().getStringExtra("namaGuest");
        btnUsername.setText(type);
        Intent intent = new Intent(this, GuestActivity.class);
        startActivity(intent);
    }
}
